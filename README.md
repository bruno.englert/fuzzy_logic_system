# Fuzzy Logic System

The project demonstrates how to control the 'inverted pendulum on a cart' problem with fuzzy control.
You can apply force to the cart with the **left/right** keys and you can disable the controller with the **enter** key.

![Screenshot](screenshot.png)
![Angle Membership](angle_membership.png)
![Angular Speed Membership](angular_speed_membership.png)

![Video](fuzzy_logic_inverted_pendulum.mp4)
