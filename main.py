import arcade

from simulation.simulation import Simulation

SCREEN_WIDTH = 1200
SCREEN_HEIGHT = 600


class Application(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height, title="Fuzzy Control Logic On Pendulum")
        arcade.set_background_color(arcade.color.WHITE)
        self.simulation = None

    def setup(self):
        """ Set up the game and initialize the variables. """
        self.set_update_rate(1. / 160)  # set fps
        self.simulation = Simulation()

    def update(self, dt):
        """ Move everything """
        # print(1/dt)
        self.simulation.step()


    def on_draw(self):
        """
        Render the screen.
        """
        arcade.start_render()
        self.simulation.draw()

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        force = 10.
        if key == arcade.key.LEFT:
            self.simulation.user_applied_force = -force
        elif key == arcade.key.RIGHT:
            self.simulation.user_applied_force = force
        elif key == arcade.key.SPACE:
            arcade.window_commands.pause(0.1)
            image = arcade.draw_commands.get_image()
            image.save('screenshot.png', 'PNG')

    def on_key_release(self, key, modifiers):
        """Called whenever a key is pressed. """
        if key == arcade.key.LEFT:
            self.simulation.user_applied_force = 0

        elif key == arcade.key.RIGHT:
            self.simulation.user_applied_force = 0

        elif key == arcade.key.ENTER:
            self.simulation.toggle_control()

    def on_close(self):
        self.simulation.pendulum.state["is_close"] = True
        super().on_close()


if __name__ == "__main__":
    window = Application(SCREEN_WIDTH, SCREEN_HEIGHT)
    window.setup()
    arcade.run()
