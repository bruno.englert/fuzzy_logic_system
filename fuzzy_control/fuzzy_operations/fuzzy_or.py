import numpy as np


def fuzzy_or(a, b, type="min"):
    if type == "min":
        return np.fmin(a, b)
