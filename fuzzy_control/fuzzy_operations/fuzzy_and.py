import numpy as np


def fuzzy_and(a, b, type="max"):
    if type == "max":
        return np.fmax(a, b)


def fuzzy_and_array(a, type="max"):
    if type == "max":
        return np.max(a, axis=0)
