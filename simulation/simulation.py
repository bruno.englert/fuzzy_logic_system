import math

import arcade
import multiprocessing as mp
import time
import numpy as np

from fuzzy_control.fuzzy_control import FuzzyControl
from simulation.pendulum import Pendulum

SCREEN_WIDTH = 1200
SCREEN_HEIGHT = 600


class Simulation:
    def __init__(self):
        self.pendulum = Pendulum(0.00005)
        self.shift = 300
        self.x0 = self.shift
        self.y0 = self.shift
        self.x1 = self.pendulum.state["x1"] + self.shift
        self.y1 = self.pendulum.state["y1"] + self.shift

        self.user_applied_force = 0

        self.fuzzy_control = FuzzyControl()
        self.is_control_active = True

        self.fuzzy_control_force = np.zeros((self.fuzzy_control.resolution,))
        self.control_force = 0.
        self.fuzzy_control.draw_member_functions()

    def step(self):
        angle = ((-self.pendulum.state["phi"] / (2 * np.pi) - 1 / 4) * 360) % 360 - 180
        angular_speed = - self.pendulum.state["omega"] / (2 * np.pi) * 360
        angle_memberships, angular_speed_memberships = self.fuzzy_control.fuzzify(angle, angular_speed)
        self.fuzzy_control_force = self.fuzzy_control.inference(angle_memberships,
                                                                angular_speed_memberships)
        self.control_force = self.fuzzy_control.defuzzify(self.fuzzy_control_force)
        if self.is_control_active:
            c_f = self.control_force
        else:
            c_f = 0.
        self.pendulum.state["force_on_cart"] = c_f + self.user_applied_force

    def toggle_control(self):
        self.is_control_active = not self.is_control_active

    def draw(self):
        """ Draw our rectangle """
        self.x0 = self.pendulum.state["x0"] + self.shift
        self.y0 = self.pendulum.state["y0"] + self.shift
        self.x1 = self.pendulum.state["x0"] + self.pendulum.length * math.cos(self.pendulum.state['phi']) + self.shift
        self.y1 = self.pendulum.length * math.sin(self.pendulum.state['phi']) + self.shift

        overflow = (self.x0 // SCREEN_WIDTH) * SCREEN_WIDTH
        self.x0 -= overflow
        self.x1 -= overflow

        # Draw cart
        arcade.draw_rectangle_outline(self.x0, self.y0, 200, 50, arcade.color.BLACK)
        # Draw pendulum
        arcade.draw_line(self.x0, self.y0, self.x1, self.y1, color=arcade.color.RED, line_width=4)
        arcade.draw_circle_filled(self.x0, self.y0, 10, arcade.color.BLACK)
        # Draw wheels
        wheel_distance = 60
        arcade.draw_circle_filled(self.x0 - wheel_distance, self.y0 - 25, 20, arcade.color.BLACK)
        arcade.draw_circle_filled(self.x0 + wheel_distance, self.y0 - 25, 20, arcade.color.BLACK)
        arcade.draw_commands.draw_arc_filled(self.x0 + wheel_distance, self.y0 - 25, 19, 19, color=[255, 255, 255],
                                             start_angle=0,
                                             end_angle=30, tilt_angle=-self.x0 * 360 / (2 * np.pi * 20))
        arcade.draw_commands.draw_arc_filled(self.x0 - wheel_distance, self.y0 - 25, 19, 19, color=[255, 255, 255],
                                             start_angle=0,
                                             end_angle=30, tilt_angle=-self.x0 * 360 / (2 * np.pi * 20))
        # Terrain
        arcade.draw_line(0, self.y0 - 45, SCREEN_WIDTH, self.y0 - 45, arcade.color.BLACK, 2)

        # Draw applied force from fuzzy control
        x = [x for x in range(self.fuzzy_control.resolution)]
        x = [0] + x + [self.fuzzy_control.resolution - 1]
        x = [(_x + 2) * 4 for _x in x]
        y = self.fuzzy_control_force.tolist()
        y = [int(_y * 200) + 5 for _y in y]
        y = [0] + y + [0]

        points = list(zip(x, y))
        arcade.draw_polygon_outline(points, [255, 191, 0, 120], 2)
        arcade.draw_text("Fuzzy control before defuzzification", self.fuzzy_control.resolution * 4 + 10, 2,
                         arcade.color.BLACK, 14)

        def draw_force(center, force, y, color, thickness=3, multiplier=3):
            if force >= 0:
                left = center
                right = np.fmin(center + force * multiplier, SCREEN_WIDTH)
            else:
                left = np.fmax(center + force * multiplier, 0)
                right = center

            arcade.draw_lrtb_rectangle_filled(left, right, y + thickness, y - thickness, color)

        draw_force(self.x0, self.control_force, self.y0 + 5, color=arcade.color.BLUSH)
        draw_force(self.x0, self.pendulum.state["friction_force"], self.y0, color=arcade.color.BLUE_GREEN)
        draw_force(self.x0, self.user_applied_force, self.y0 - 5, color=arcade.color.YELLOW_ORANGE)

        text_y0 = 5
        text_y_diff = 22
        width = 500
        arcade.draw_text(
            text="{:0.2f}N Control force".format(self.control_force),
            start_x=SCREEN_WIDTH - 5,
            start_y=text_y0,
            color=arcade.color.BLUSH,
            font_size=14,
            align='right',
            anchor_x='right',
            width=width)
        arcade.draw_text(
            text="{:0.2f}N Friction force".format(self.pendulum.state["friction_force"]),
            start_x=SCREEN_WIDTH - 5,
            start_y=text_y0 + text_y_diff * 1,
            color=arcade.color.BLUE_GREEN,
            font_size=14,
            align='right',
            anchor_x='right',
            width=width)
        arcade.draw_text(
            text="{:0.2f}N User applied force".format(self.user_applied_force),
            start_x=SCREEN_WIDTH - 5,
            start_y=text_y0 + text_y_diff * 2,
            color=arcade.color.YELLOW_ORANGE,
            font_size=14,
            align='right',
            anchor_x='right',
            width=width)
        arcade.draw_text(
            text="{:0.2f}° Angle".format(((-self.pendulum.state["phi"] / (2 * np.pi) - 1 / 4) * 360) % 360 - 180),
            start_x=SCREEN_WIDTH - 5,
            start_y=text_y0 + text_y_diff * 3,
            color=arcade.color.BLACK,
            font_size=14,
            align='right',
            anchor_x='right',
            width=width)
        arcade.draw_text(
            text="{:0.2f}°/s Angular speed".format(-self.pendulum.state["omega"] / (2 * np.pi) * 360),
            start_x=SCREEN_WIDTH - 5,
            start_y=text_y0 + text_y_diff * 4,
            color=arcade.color.BLACK,
            font_size=14,
            align='right',
            anchor_x='right',
            width=width)
