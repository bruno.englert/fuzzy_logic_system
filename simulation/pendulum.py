import multiprocessing as mp
import numpy as np
from numba import jit
from numba import prange


@jit(nopython=True)
def _step(dt, friction, x0, phi, omega, cart_speed, force_on_cart, pre_A, pre_B, pre_C):
    for _ in prange(100):
        cosphi = np.cos(phi)
        sinphi = -np.sin(phi)
        friction_force = -friction * cart_speed ** 2. * np.sign(cart_speed)

        cart_acc = ((cosphi * pre_A * omega ** 2 + force_on_cart + friction_force) * pre_B -
                    (pre_A * -9.81 * cosphi) * (pre_A*sinphi)) / \
                   (pre_C * pre_B - pre_A*sinphi*pre_A*sinphi)

        beta = (-cart_acc * pre_A * sinphi + (pre_A * -9.81 * cosphi)) / pre_B

        x0 += cart_speed * dt
        cart_speed += cart_acc * dt
        phi += dt * omega
        omega += dt * beta

    return x0, phi, omega, cart_speed, friction_force


class Pendulum:
    def __init__(self, dt, length=200., pendulum_mass=0.5, cart_mass=1., friction=0.0008):
        self.dt = dt
        self.length = length
        self.pendulum_mass = pendulum_mass
        self.cart_mass = cart_mass
        self.total_mass = (self.cart_mass + self.pendulum_mass)
        self.polemass_length = (self.pendulum_mass * self.length)
        self.friction = friction
        self.rotation_friction = friction

        self.manager = mp.Manager()
        self.state = self.manager.dict()
        self.state["x0"] = 0.
        self.state["y0"] = 0.
        self.state["x1"] = 0.
        self.state["y1"] = 0.
        self.state["phi"] = np.pi / 2. + 1e-1  # angle
        self.state["omega"] = 0.  # angular speed
        self.state["cart_speed"] = 0.
        self.state["friction_force"] = 0.
        self.state["force_on_cart"] = 0.
        self.state["is_close"] = False

        p = mp.Process(target=self.step, args=(self.state,))
        p.daemon = True
        p.start()

    def step(self, state):
        pre_A = self.pendulum_mass * self.length
        pre_B = self.length ** 2 * self.pendulum_mass + self.pendulum_mass * (self.length / 2.) ** 2
        pre_C = self.cart_mass + self.pendulum_mass

        while not state["is_close"]:
            state["x0"], \
            state["phi"], \
            state["omega"], \
            state["cart_speed"], \
            state["friction_force"] = _step(self.dt,
                                            self.friction,
                                            state["x0"],
                                            state["phi"],
                                            state["omega"],
                                            state["cart_speed"],
                                            state["force_on_cart"],
                                            pre_A,
                                            pre_B,
                                            pre_C)
